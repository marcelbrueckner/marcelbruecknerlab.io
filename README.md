# Personal site configuration

This is the configuration repository for my identity page (and maybe blog) at [marcelbrueckner.de](https://marcelbrueckner.de).

The page is built with [Hugo](https://gohugo.io/), *one of the most popular open-source static site generators* and uses my [own fork](https://github.com/marcelbrueckner/hugo-theme-massively.git) of Curtis Timson's [Hugo Theme Massively](https://github.com/curtistimson/hugo-theme-massively), based on [Massively](https://html5up.net/uploads/demos/massively/index.html) by [HTML5UP](https://html5up.net).
