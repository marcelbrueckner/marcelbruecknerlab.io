+++
title = "Marcel Brückner"
linktitle = "About"
description = "DevOps Engineer · Software Developer · IoT and Smart Home Enthusiast"
avatar = "images/avatar.jpg"
+++

Devops Engineer with several years of experience in integration of heterogeneous systems as well as automation of business and software development processes. I also pursue my passion for technology and automation in personal, mostly smaller projects.
