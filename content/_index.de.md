+++
title = "Marcel Brückner"
linktitle = "Über"
description = "DevOps Engineer · Softwareentwickler · IoT- und Smart-Home-Enthusiast"
avatar = "images/avatar.jpg"
+++

DevOps Engineer mit mehreren Jahren Erfahrung sowohl in der Integration heterogener Systeme als auch der Automatisierung von Geschäfts- und Softwareentwicklungsprozessen. Meiner Leidenschaft für Technologie und Automatisierung gehe ich auch in persönlichen, zumeist kleineren Projekten nach.
